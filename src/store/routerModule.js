const routeModule = {
  state: {
    path: null,
  },
  getters: {
    currentPath: (state) => state.path,
  },
  actions: {
    setPath: ({ commit }, path_name) => commit("SET_PATH", path_name),
  },
  mutations: {
    SET_PATH: (state, path_name) => {
      state.path = path_name;
    },
  },
};

export default routeModule;
