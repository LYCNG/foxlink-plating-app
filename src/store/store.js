import { createStore } from "vuex";
import routeModule from "./routerModule";
import userModule from "./userModule";

export default createStore({
  modules: {
    userState: userModule,
    pathState: routeModule,
  },
});
