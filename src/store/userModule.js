const user = {
  admin: { password: "fs2022", apart: 0 },
  1881727: { password: "1881727", apart: 1 },
  1894966: { password: "1894966", apart: 1 },
  675818: { password: "675818", apart: 2 },
  617294: { password: "617294", apart: 2 },
  1882308: { password: "1882308", apart: 3 },
  1898821: { password: "1898821", apart: 3 },
  1888553: { password: "1888553", apart: 3 },
  1891164: { password: "1891164", apart: 3 },
  1892572: { password: "1892572", apart: 3 },
  1890425: { password: "1890425", apart: 4 },
  1884352: { password: "1884352", apart: 4 },
  4884: { password: "4884", apart: 0 },
  foxlink: { password: "foxlink2022", apart: 0 },
};
const userModule = {
  state: {
    isAuth: false,
    apart: 0,
    username: "",
    foxToken: "",
    error: "",
  },
  getters: {
    isAuth: (state) => state.isAuth,
    getApart: (state) => state.apart,
    getError: (state) => state.error,
  },
  actions: {
    setAuth: ({ commit }, auth) => commit("SET_AUTH", auth),
    login: ({ commit }, data) => commit("LOGIN_START", data),
    initError: ({ commit }) => commit("INIT_ERROR"),
    logout: ({ commit }) => commit("LOGOUT_START"),
  },
  mutations: {
    SET_AUTH: (state, auth) => {
      state.isAuth = auth; //set auth be true
    },
    LOGIN_START: (state, data) => {
      const { username, password } = data;

      if (user[username] && password === user[username].password) {
        let token = "fox_link_token_" + username;
        state.isAuth = true;
        state = {
          ...state,
          isAuth: true,
          username: username,
          foxToken: token,
          apart: user[username].apart,
        };
        let storageValue = JSON.stringify(state);
        localStorage.setItem("fox_link_token", storageValue);
      } else {
        let err_msg = "登入失敗";
        state.error = err_msg;
      }
    },
    LOGOUT_START: (state) => {
      localStorage.removeItem("fox_link_token");
      state.isAuth = false;
    },
    INIT_ERROR: (state) => {
      console.log(state.error);
      state.error = "";
    },
  },
};

export default userModule;
