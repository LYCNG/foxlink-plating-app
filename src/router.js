import { createRouter, createWebHistory } from "vue-router";

import FeedPage from "./components/feed-component/FeedPage.vue";
import FeedRatioPage from "./components/feed-component/FeedRatioPage.vue";
import FeedReplacePage from "./components/feed-component/FeedReplacePage.vue";
import HistoryChart from "./components/home-history/HistoryChart.vue";
import HistoryTable from "./components/home-history/HistoryTable.vue";
import SizeMeasurement from "./components/home-history/SizeMeasurement.vue";
import Home from "./components/Home.vue";
import IQCPage from "./components/iqc-component/IQCPage.vue";
import Login from "./components/Login.vue";
import MoisturePage from "./components/moisture-component/MoisturePage.vue";
import MoldMaintain from "./components/mold-component/MoldMaintain.vue";
import MoldRepair from "./components/mold-component/MoldRepair.vue";
import MoldReplace from "./components/mold-component/MoldReplace.vue";
import MonitorDouble from "./components/monitor-components/MonitorDouble.vue";
import MonitorParamsTable from "./components/monitor-components/MonitorParams.vue";
import MonitorSingleForm from "./components/monitor-components/MonitorSingle.vue";
import ToastPage from "./components/ToastPage.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/login", name: "Login", component: Login },
    { path: "/", name: "Home", component: Home },
    { path: "/home", name: "Home", component: Home },
    { path: "/home/history", name: "History", component: HistoryChart },
    { path: "/home/summary", name: "Summary", component: HistoryTable },
    { path: "/home/size", name: "Size", component: SizeMeasurement },

    { path: "/monitor-params", name: "Monitor", component: MonitorParamsTable },
    { path: "/monitor-single", name: "Monitor1", component: MonitorSingleForm },
    { path: "/monitor-double", name: "Monitor2", component: MonitorDouble },

    { path: "/iqc-page", name: "IQCPage", component: IQCPage },
    { path: "/feed-record", name: "FeedPage", component: FeedPage },
    {
      path: "/feed-replace",
      name: "FeedReplacePage",
      component: FeedReplacePage,
    },
    {
      path: "/feed-ratio",
      name: "FeedRatioPage",
      component: FeedRatioPage,
    },
    { path: "/feed-record", name: "FeedPage", component: FeedPage },
    { path: "/moisture-record", name: "MoisturePage", component: MoisturePage },
    { path: "/mold-maintain", name: "MoldMaintain", component: MoldMaintain },
    { path: "/mold-replace", name: "MoldReplace", component: MoldReplace },
    { path: "/mold-repair", name: "MoldRepair", component: MoldRepair },
    { path: "/test", name: "Test", component: ToastPage },
  ],
});
export default router;
