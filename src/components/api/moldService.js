import axios from "axios";
import axiosFox from "./api";

export default class MoldService {
  getMemberData = async (apart) => {
    /** 
        取得成員
        @return data {name:string,id:string}[]
            */
    return axiosFox.get("api/member/" + apart).then((data) => {
      if (!data.data.data) {
        return { data: [] };
      }
      return data.data;
    });
  };
  getMoldMember1 = async () => {
    return axiosFox.get("api/member/mold/1").then((data) => {
      if (!data.data) {
        return { data: [] };
      }
      return data.data;
    });
  };
  getMoldMember2 = async () => {
    return axiosFox.get("api/member/mold/2").then((data) => {
      if (!data.data) {
        return { data: [] };
      }
      return data.data;
    });
  };
  getMachine = async () => {
    // ```
    //     取得成型機台
    //     return data {id:string}[]
    // ```;
    return await axiosFox
      .get("api/replacematerial/query-equipment")
      .then((data) => data);
  };
  getOldMold = async () => {
    // ```
    //         return {id:string}[]
    //     ```;
    return await axiosFox
      .get("api/replacementmold/get-old-mold")
      .then((data) => {
        return data;
      });
  };
  getNewMold = async () => {
    // ```
    //         return {id:string}[]
    //     ```;
    return await axiosFox
      .get("api/replacementmold/get-new-mold")
      .then((data) => data);
  };
  postMaintainData = async (body) => {
    //  ```
    //     body data: {
    //         member:string,
    //         machine:string,
    //         from:string,
    //         to:string
    //     }[]
    //     return
    //         success:{success:true,message:successMessage},
    //         failed:{success:false,message:failedMessage}
    // ```;
    return axiosFox
      .post("api/moldmaintena/add", body)
      .then((data) => data.data);
  };
  postReplaceData = async (body) => {
    // ```
    //     body data: {
    //         member:string,
    //         machine:string,
    //         from:string,
    //         to:string
    //         slot1~slot4
    //     }[]
    //     return
    //         success:{success:true,message:successMessage},
    //         failed:{success:false,message:failedMessage}
    // ```;
    return axiosFox
      .post("/api/replacementmold/add", body)
      .then((data) => data.data);
  };
  postRepairData = async (body) => {
    return axiosFox.post("/api/moldrepair/add", body).then((data) => data.data);
  };

  postSearchRepairData = async (body) => {
    return axiosFox.post("/api/moldrepair/search", body).then((data) => data);
  };
  postSearchMaintainData = async (body) => {
    // 送出查詢
    // body:data:{
    //     machine:string,
    //     from:timeString,
    //     to:timeString
    // },
    // return data: {
    //     uid:string,
    //     member:string,
    //     machine:string,
    //     from:string,
    //     to:string,
    //     recordTime:string
    // }[]
    return axiosFox.post("api/moldmaintena/search", body).then((data) => data);
    // return axiosFox.get("api/moldmaintena/search", body).then((data) => data);
  };

  postSearchReplaceMold = async (body) => {
    // ```
    //     送出查詢
    //     body:data:{
    //         machine:string,
    //         from:timeString,
    //         to:timeString
    //     },
    //     return data: {
    //         uid:string,
    //         member:string,
    //         machine:string,
    //         oldMold:string,
    //         newMold:string,
    //         from:string,
    //         to:string,
    //         recordTime:string
    //     }[]
    // ```;
    return axiosFox
      .post("api/replacementmold/search", body)
      .then((data) => data.data);
  };
}
