import axiosFox from "./api";

export default class MonitorService {
  /**
   * * 取得成員列表
   * @param {*} apart 部門代號0|1|2|3|4
   * @returns
   */
  getMemberData = async (apart) => {
    return axiosFox.get("api/member/" + apart).then((data) => {
      if (!data.data.data) {
        return { data: [] };
      }
      return data.data;
    });
  };
}
