import axiosFox from "./api";

export default class MoistureService {
  getMemberData = async (apart) => {
    /**
     * * 取得成員
     * @return data {name:string,id:string}[]
     */
    return axiosFox.get("api/member/" + apart).then((data) => {
      if (!data.data.data) {
        return { data: [] };
      }
      return data.data;
    });
  };
  getPatNumber = async () => {
    /**
     *  * 取得原料料號
     *  @return data {id:string}[]
     */

    return await axiosFox.get("api/material").then((data) => data);
  };
  getMachine = async () => {
    /**
           * 取得成型機台
          @return data {id:string}[]
       */
    return await axiosFox
      .get("api/replacematerial/query-equipment")
      .then((data) => {
        return data;
      });
  };
  getProductNumber = async () => {
    /**
       * 取得產品料號
      @return data {id:string}[]
       */
    return await axiosFox
      .get("api/replacematerial/query-product")
      .then((data) => data);
  };
  postData = async (body) => {
    /**
     * 新增水分測試記錄
          body data: {
              member:string,
              machine:string,
              productNumber:string,
              patNumber:string,
              sampling:string,
              samplingTime:string,
              testTime:string
          }[]
     */
    return await axiosFox
      .post("api/moisture/add", body)
      .then((data) => data.data);
  };
  postSearchData = async (body) => {
    /**
          送出查詢
          body:data:{
              machine:string,
              productNumber:string
              from:timeString,
              to:timeString
          },
          @return data: {
              uid:string,
              member:string,
              machine:string,
              productNumber:string,
              patNumber:string,
              sampling:string,
              samplingTime:string,
              testTime:string
              recordTime:string
          }[]
    */
    return await axiosFox
      .post("api/moisture/search", body)
      .then((data) => data.data);
  };
}
