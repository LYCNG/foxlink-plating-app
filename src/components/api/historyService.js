import axiosFox from "./api";
import moment from "moment";

const timeFormat = (text) => moment(text).format("YYYYMMDDHHHHmm");

export default class HistoryService {
  getHistoryData = async (type, from, to) => {
    const fromTime = timeFormat(from);
    const toTime = timeFormat(to);
    return axiosFox
      .get(`/api/mainpage/history/${type}/${fromTime}-${toTime}`)
      .then((data) => data);
  };
  getSummary = async () => {
    return axiosFox.get("/api/mainpage/summary").then((data) => data);
  };

  getMeasure = async (from, to) => {
    const fromTime = timeFormat(from);
    const toTime = timeFormat(to);
    return axiosFox
      .post("api/mainpage/measure", {
        from: fromTime,
        to: toTime,
      })
      .then((data) => data);
  };
}
