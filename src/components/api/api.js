import axios from "axios";
const local = window.location.origin;
const test = "http://192.168.0.37:5050/";

const axiosFox = axios.create({
  baseURL: test,
  header: {
    "Content-Type": "application/x-www-form-urlencoded",
    // "Access-Control-Allow-Methods": "GET,POST,PUT,PATCH,DELETE,HEAD,OPTIONS",
    // "Access-Control-Allow-Headers":
    //   "Content-Type, Origin, Accept, Authorization, Content-Length, X-Requested-With,x-www-form-urlencoded",
  },
});

export default axiosFox;
