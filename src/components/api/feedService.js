import moment from "moment";
import axiosFox from "./api";

const timeFormat = (text) => moment(text).format("YYYY-MM-DD");

export default class FeedService {
  getMemberData = async (apart) => {
    /** 
        取得成員
        @return data {name:string,id:string}[]
            */
    return axiosFox.get("api/member/" + apart).then((data) => {
      if (!data.data.data) {
        return { data: [] };
      }
      return data.data;
    });
  };

  getPatNumber = async () => {
    /** 
        取得原料料號
        @return data {id:string}[]
        */
    return axiosFox.get("api/material").then((data) => data);
  };
  getLotNumber = async (body) => {
    /** 
            取得原料批號
            body: data:{id:string} #原料料號id
            return data {id:string}[]
            */
    return axiosFox.get("api/material/query-lot").then((data) => data);
  };

  getMachine = async () => {
    // ```
    //     取得成型機台
    //     return data {id:string}[]
    // ```;
    return await axiosFox
      .get("api/replacematerial/query-equipment")
      .then((data) => {
        if (!data) {
          return [];
        }
        return data;
      });
  };
  getProductNumber = async () => {
    // ```
    //     取得產品料號
    //     return data {id:string}[]
    // ```;
    return await axiosFox
      .get("api/replacematerial/query-product")
      .then((data) => {
        return data;
      });
  };
  postReplaceData = async (body) => {
    /**
            送出加料更換批號記錄
            body:data:{
                member:string,
                machine:string,
                productNumber:string,
                patNumber:string,
                lotNumber:string,
                batchChangeTime:string,
            }
            @return
                success:{success:true,message:successMessage},
                failed:{success:false,message:failedMessage}
    */

    return await axiosFox
      .post("api/replacelot/add", body)
      .then((data) => data.data);
  };
  postRatioData = async (body) => {
    /**
            送出加料換次料比記錄
            @params
            body:data:{
                 member:string,
                 machine:string,
                 productNumber:string,
                 patNumber:string,
                 lotNumber:string,
                 changeRatio:number|string,
                 batchChangeTime:string,
            }
            @return
                success:{success:true,message:successMessage},
                failed:{success:false,message:failedMessage}
    */
    console.log(body);
    return await axiosFox
      .post("api/replacematerial/add", body)
      .then((data) => data.data);
  };
  postSearchReplaceData = async (body) => {
    /**
             更換批號記錄查詢
            body:data:{
                machine:string,
                productNumber:string
                from:timeString,
                to:timeString
            },
            @return data: {
                uid:string,
                member:string,
                machine:string,
                productNumber:string,
                patNumber:string,
                lotNumber:string,
                batchChangeTime:string,
                recordTime:string
            }[]
    */

    return await axiosFox
      .post(`api/replacelot/search`, body)
      .then((data) => data.data);
  };
  postSearchRatioData = async (body) => {
    /**
            更換次料比記錄查詢
            body:data:{
                machine:string,
                productNumber:string
                from:timeString,
                to:timeString
            },
            @return data: {
                uid:string,
                member:string,
                machine:string,
                productNumber:string,
                patNumber:string,
                lotNumber:string,
                changeRatio:number|string,
                batchChangeTime:string,
                recordTime:string
            }[]
    */

    return await axiosFox
      .post(`api/replacematerial/search`, body)
      .then((data) => data.data);
  };
}
