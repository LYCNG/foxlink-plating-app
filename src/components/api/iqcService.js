import axiosFox from "./api";
import moment from "moment";

const timeFormat = (text) => moment(text).format("YYYY-MM-DD");
export default class IQCService {
  getMemberData = async (apart) => {
    /** 
    取得成員
     @return data {name:string,id:string}[]
        */
    return axiosFox.get("api/member/" + apart).then((data) => {
      if (!data.data.data) {
        return { data: [] };
      }
      return data.data;
    });
  };
  getPatNumber = async () => {
    /** 
    取得原料料號
    @return data {id:string}[]
    */
    return await axiosFox.get("api/material").then((data) => data);
  };
  getLotNumber = async (body) => {
    /** 
        取得原料批號
        body: data:{id:string} #原料料號id
        return data {id:string}[]
        */
    return await axiosFox.get("api/material/query-lot").then((data) => data);
  };
  postData = async (body) => {
    /** 
            送出
            body data: {
                patNumber:string,
                lotNumber:string,
                p1:string|float ,
                p2:string|float ,
                p3:string|float ,
                p4:string|float ,
                p5:string|float ,
                average:string|float, 
                member:string,
            }[]
            return 
                success:{success:true,message:successMessage},
                failed:{success:false,message:failedMessage}
        */
    return axiosFox
      .post("api/material/inspect-add", body)
      .then((data) => data.data);
  };
  postSearch = async (body) => {
    /** 
        送出查詢
        body:data:{
            from:timeString,
            to:timeString
        },
        @return data: {
            uid:string,
            patNumber:string,
            lotNumber:string,
            p1:string|float ,
            p2:string|float ,
            p3:string|float ,
            p4:string|float ,
            p5:string|float ,
            average:string|float, 
            member:string,
            date:string
        }[]
    */
    return axiosFox
      .post(`api/material/inspect-search`, body)
      .then((data) => data.data);
  };
}
