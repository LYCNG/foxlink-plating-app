import axiosFox from "./api";
import moment from "moment";

const timeFormat = (text) => moment(text).format("YYYY-MM-DD");

export default class HomeService {
  getHomeData = async () => {
    return axiosFox.get("/api/mainpage").then((data) => data);
  };
}
