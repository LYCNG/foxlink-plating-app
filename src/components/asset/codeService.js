const CodeService = {
  member: "登打人員",
  patNumber: "原料料號",
  lotNumber: "原料批號",
  machine: "成型機台",
  productNumber: "產品料號",
  batchChangeTime: "次料變更時間",
  lotChangeTime: "批號變更時間",
  from: "起始時間",
  to: "結束時間",
  samplingTime: "抽樣時間",
  testTime: "測試時間",
  oldMold: "舊模具編號",
  newMold: "新模具編號",
  slot1: "模穴1",
  slot2: "模穴2",
  slot3: "模穴3",
  slot4: "模穴4",
};
export default CodeService;
